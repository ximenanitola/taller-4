#Taller #4: Dado un conjunto de valores en una 
#lista de 1000 valores generados aleatoriamente entre 0 y 20
#1. Crear una función que devuelva con valor 
#True o False dependiendo si el valor de la lista original es primo o no
#2. Crear una función que devuelva la media de la lista
#3. Crear una función que devuelva el factorial de cada número de la lista
#4. Crear una función que retorne el valor mayor y menor de la lista

import random
import pandas as pd
import math

def generar_lista(inf, sup, cantidad):
    Lista=[]
    for i in range(0, cantidad):
        valor=random.randrange(inf, sup)
        Lista.append(valor)
    return Lista

#print(generar_lista(0, 20, 1000))

def verificar_primo(numero):
    if numero>1:
        for i in range(2, numero):
            if(numero%i ==0):
                return False
        return True
    else:
        return False

def definir_primos(Lista):
    Lista_resultado=[]
    for i in Lista:
        resultado=verificar_primo(i)
        Lista_resultado.append(resultado)
    return Lista_resultado

miLista=generar_lista(0, 20, 1000)
#print(miLista)
#print(definir_primos(miLista))

def media(Lista):
    serie=pd.Series(Lista)
    return serie.mean()

#miLista=generar_lista(0, 20, 1000)    
#print(miLista)
#print("La media es: ", media(miLista))

def Factorial(Lista):
    Lista_resultado=[]
    for i in Lista:
        valor=math.factorial(i)
        Lista_resultado.append(valor)
    return Lista_resultado

#miLista=generar_lista(0, 20, 1000)    
#print(miLista)
#print("Los factoriales de cada valor son: ", Factorial(miLista))

def Mayor_menor(Lista):
    menor=min(Lista)
    mayor=max(Lista)
    return [mayor, menor]

#miLista=generar_lista(0, 20, 1000)    
#print(miLista)
#print("El número mayor y menor es: ", Mayor_menor(miLista))            